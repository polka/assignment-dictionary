%define last_word 0
%macro colon 2
	%ifnstr %1
		%error "The 1st arg must be a string"
	%endif
	%ifnid %2
		%error "The 2nd arg must be an id"		
	%endif
	
	%2: dq last_word
	db %1, 0
	
%define last_word %2
%endmacro

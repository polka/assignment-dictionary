.PHONY: all
all: main

main.o: main.asm colon.inc words.inc lib.inc
	nasm -f elf64 -o $@ $<

%.o: %.asm
	nasm -f elf64 -o $@ $<
	
.PHONY: main
main: main.o lib.o dict.o error.o
	ld -o $@ $?


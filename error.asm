%include "lib.inc"
global i_error

key: db "There isnt this key in the dictionary", 10
input: db "Input error", 10

i_error:
	cmp rdi, rdi
	je .input
.key:
	mov rdi, key
	jmp .print
.input:
	mov rdi, input
.print:
	push rsi
	call string_length
	mov rdx, rax
	pop rsi
	
	mov rsi, rdi
	mov rdi, 1
	mov rax, 1
	syscall
	
	ret

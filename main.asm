%include "lib.inc"
%include "colon.inc"
%include "words.inc"
extern find_word
extern i_error

global _start

section .rodata
key_enter: db "Input key: ", 0
value_found: db "Found value: ", 0

section .text
_start:
	mov rdi, key_enter
	call print_string
	sub rsp, 256		;buff size
	mov rdi, rsp
	mov rsi, 256
	call read_word
	test rax, rax		;0 => error
	jz .input_error
	mov rdi, rax
	mov rsi, last_word
	call find_word
	test rax, rax
	jz .key_error
	add rax, 8		;addr
	mov rdi, rax
	
	push rdi
	call string_length	;key length
	pop rdi
	
	inc rdi
	add rdi, rax		;length + 1
	
	push rdi
	mov rdi, value_found
	call print_string
	pop rdi
	
	call print_string
	call print_newline
	jmp .exit

.key_error:
	mov rdi, 1
	jmp .print
.input_error:
	xor rdi, rdi
.print
	call i_error
.exit:
	add rsp, 256		;buff size
	call exit



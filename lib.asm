section .text

global string_length
global print_string
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy
global exit


;terminates the process
exit:
	mov rax, 60			;num of syscall (exit)
	xor rdi, rdi
	syscall
	ret
	
;takes a pointer to a null-terminated string and returns it's length
;need to save rax before call it
string_length:
    	xor rax, rax			;clean rax
    .count:
    	cmp byte [rdi+rax], 0		;check if the current symbol is null-terminator

    	je .end			;jump if we found null-terminator
    	inc rax			;go to next symbol and counter++
    	jmp .count
    	
    .end:
    	ret				;here rax should have return value
    	
;takes a pointer to a null-terminated string and prints it to stdout
;need to save rax before call it
print_string:
	push rdi			;saving rdi (callee-saved)
	push rsi			;saving rsi (callee-saved)
	
	push rax			;saving rax (caller-saved) 
    	call string_length
    	mov rdx, rax			;length - arg3
    	pop rax
    	
    	mov rsi, rdi			;arg2
    	mov rax, 1			;num of syscall (write)
    	mov rdi, 1			;descriptor (stdout)
    	syscall
    	
    	pop rsi
    	pop rdi
    	ret

;takes a character code and prints it to stdout
;need to save rax and rdx before call it
print_char:
	push rdi			;saving rdi (callee-saved)
	push rsi			;saving rsi (callee-saved)

	mov rdx, 1			;length (one symbol)
	mov rax, 1			;num of syscall (write)
	
    	mov rsi, rdi			;arg
    	
    	mov rdi, 1			;descriptor (stdout)		
    	syscall
    	
    	pop rsi
    	pop rdi
    	ret
    	
;line break
;use print_char
print_newline:
	
	push rdi			;saving rdi (callee-saved)
	
	mov rdi, 0xA			;char code
	
	push rax			;saving rax (caller-saved)
	push rdx			;saving rdx (caller-saved)
	call print_char
	pop rdx
	pop rax
	
	pop rdi
	ret
	
;prints an unsigned 8-byte number in decimal format
;need to save rax, rcx, rdx and r8 before call it
print_uint:
	push rdi			;saving rdi (callee-saved)

	mov rax, rdi			;arg1
	mov rcx, 10			;divider (for decimal format)
	mov r8, rsp			;save stack state (callee-saved)
	push 0				;null-terminator
    .loop:
	xor rdx, rdx			;clean rdx
	div rcx
	add rdx, 48			;turn to ASCII (cause 48 is code of 0) 
	
	dec rsp
	mov [rsp], dl			;mod (save it in addr in rsp)
	
	cmp byte [rax], 0		;check if the current symbol is null-terminator

    	je .end			;jump if we found null-terminator
    	jmp .loop
    	
    .end:
    	mov rdi, rsp			;pointer to our string (save it in rdi as argument)
    	push rax			;saving rax (caller-saved)
    	
    	call print_string
    	
    	pop rax
    	mov rsp, r8			;turn the state of the stack back
    	pop rdi
    	ret
	
	
;prints a signed 8-byte number in decimal format
print_int:
	push rdi			;saving rdi (callee-saved)
	push rax			;saving rax (caller-saved)
	push rdx			;saving rdx (caller-saved)
	push rcx			;saving rcx (caller-saved)
	push r8			;saving r8 (caller-saved)
	
	cmp rdi, 0
	jae .print			;if above or equal
	mov rdi, 0x2D			;char ~ minus
	
	call print_char
	
	neg rdi
	
    .print:
    	call print_uint
    	
    	pop r8
    	pop rcx
    	pop rdx
	pop rax
	pop rdi
    	ret
    	
				
;takes two pointers to null-terminated strings and returns 1 (equal) or 0 (not equal)
;rdi - (first arg) pointer to string 1, rsi - (second arg) pointer to string 2
;need to save rax, rcx and r8 before call it
string_equals:
	xor rax, rax			;return value
	xor rcx, rcx 			;clean for loop (counter)
	xor r8, r8
	
    .loop:
    	mov r8b, byte [rdi+rcx]
    	cmp r8b, byte [rsi+rcx]	;check if equal
    	
    	jne .return			;if not equal - return 0
    	
    	cmp r8b, 0
    	mov rax, 1			;if equal - return 1
    	jne .return		
    	
    	inc rcx
    	jmp .loop
    	
    .return:
    	ret
    	

;reads one symbol from stdin and return it or 0 (if it's the end of the stream)
;need to save rax and rdx before call it
read_char:
	push rdi			;saving rdi (callee-saved)
	push rsi			;saving rsi (callee-saved)
	
	push 0
	xor rax, rax			;num of syscall (read)
	xor rdi, rdi			;descriptor (stdin)
	mov rdx, 1			;length (one symbol)
	mov rsi, rsp			;arg (source index)
	syscall
	pop rax			;return value (0 or symbol)
	
	pop rsi
	pop rdi
	ret
	

;takes the addr of the start of the buffer and buffer's size
;reads a word from stdin into the buffer (skipping whitespace at the beginning)
;whitespace = 0x20, tab = 0x9, line break = 0xA
;stops and returns 0, if the word is too big for the buffer
;success = returns buffer's addr in rax, word's length in rdx
;no success = 0 in rax
;have to add a null-terminator to the word

;rdi - (first arg) addr of the start of the buffer
;rsi - (second arg) buffer's size

;need to save rax and rdx before call it
read_word:
  .space:
    xor rdx, rdx
    push rdi
    push rsi
    push rdx
    call read_char
    pop rdx
    pop rsi
    pop rdi
    
    test rax, rax
    je .exit
    
    test rsi, rsi
    je .exit

    cmp rax, 0x20
    jz .space
    
    cmp rax, 0x9
    jz .space
    
    cmp rax, 0xA
    jz .space
    
    jmp .write
 .read:
    push rdi
    push rsi
    push rdx
    call read_char
    pop rdx
    pop rsi
    pop rdi
    
    test rax, rax
    je .null
    
    test rsi, rsi
    je .null
    
    cmp rax, 0x20
    jz .null

    cmp rax, 0x9
    jz .null
    
    cmp rax, 0xA
    jz .null
    
  .write:
    mov byte[rdx + rdi], al
    inc rdx
    cmp rdx, rsi
    jz .exit
    jmp .read
  .null:
    mov byte[rdx + rdi], 0
    mov rax, rdi
    ret
  .exit:
    xor rax, rax
    xor rdx, rdx
    ret
    		

;takes a pointer to string
;tries to read from it unsigned number
;returns number in rax, length (num of symbols) in rdx
;rdx = 0, if no success in reading the number
;need to save rax, rcx and rdx before call it
parse_uint:
	xor rdx, rdx			;counter (keep length)
	xor rax, rax
	xor rcx, rcx
    .loop:
    	mov cl, byte [rdi+rdx]
    	sub rcx, 48			;turn from ASCII to num
    	
    	cmp cl, 0			;check if it's a number
    	jl .end			;for signed cause ASCII
    	cmp cl, 9
    	jg .end
    	
    	inc rdx			;counter++
    	
    	imul rax, 10 			;left shift (for the next num)
    	add rax, rcx
    	jmp .loop
    	
   .end:
   	ret
    		

;takes a pointer to string
;tries to read from it signed number
;no space between sign and number
;returns number in rax, length (num of symbols, including sign) in rdx
;rdx = 0, if no success in reading the number
;need to save rax and rdx before call it
parse_int:
	cmp byte[rdi], '-'
	jne .unsigned
	
	inc rdi			;cause signed (inc + neg = transformation)
	
	push rcx			;saving rcx (caller-saved)
	call parse_uint
	pop rcx
	
	test rdx, rdx
	je .end
	inc rdx			;increase length (cause of sign)
	neg rax			;neg returned number
	dec rdi			;callee-saved
	ret
	
    .unsigned:
    	call parse_uint
    .end:
    	ret
	

;takes a pointer to string, a pointer to buffer and buffer's length
;copies string into the buffer
;returns string's length if it's fits into the buffer or 0 if not

;rdi - (first arg) pointer to string
;rsi - (second arg) pointer to the buffer
;rdx - (third arg) buffer's size
string_copy:
	xor rcx, rcx			
	xor rax, rax
    .loop:
    	cmp rax, rdx			;check if too big for the buffer
    	je .overflow
    	
    	mov cl, byte [rax+rdi]
    	mov byte [rax+rsi], cl		;write to the buffer
    	cmp byte [rax+rsi], 0
    	je .end
    	inc rax
    	 
    	jmp .loop
    	
    .overflow:
    	xor rax, rax
    .end:
    	ret
	
	
	
	
				
	
	
	
	
	

	

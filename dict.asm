global find_word
extern string_equals

section .text

;find particular key in dictionary
;if key was found - returns addr
;else - returns 0
;rdi = str (key)
;rax = return value (addr or 0)
find_word:

	test rsi, rsi		;check if the last word in dictionary
	jz .end

	push rdi
	push rsi
	
	add rsi, 8
	call string_equals
	
	pop rsi
	pop rdi
	
	test rax, rax		;check if string are equal
	jnz .end
	mov rsi, [rsi]
	
	jmp find_word
	
.end:
	mov rax, rsi
	ret

